# Projet fil rouge

![Dokuwiki](https://gitlab.com/Adrenokrome/int-syst/uploads/4591b8d8176065297c98469546bed287/Dokuwiki.PNG "Dokuwiki")

## Serveur WEB avec Dokuwiki à distance

Mon projet consiste à me connecter à un serveur Web à distance.  
Je dois configurer l'accès à distance d'un wiki sur une VM CentOS virtualisé sur une machine WINDOWS 10.  
Je me connecterai via mon laptop Omen sur mon serveur web.  

Voici la topologie de mon réseau :   
![Topologie](https://gitlab.com/Adrenokrome/int-syst/uploads/05e5c959642e7ad8ba016dbf0eaff1d7/Architecture.png "Topologie")

-----------------------------------------------------

### Installation de la machine CentOS     

- Téléchargement de l'iso CentOS Server     
- Installation de la VM sur VirtualBox    
- Suppression du mode veille sur ma VM et mon hôte      

![Virtual_Box](/uploads/a8c951409a23f8a8062003b8a8834016/Virtual_Box.PNG "Virtual_Box")

------------------------------------------------------

### Configuration de CentOS et de ses cartes réseaux        

- Mise à jour de l'OS 

    yum update     
    yum upgrade    

- Edition de la carte réseaux   

Modifier le fichier /etc/sysconfig/network-scripts/ifcfg-enp0s3  

![Network](https://gitlab.com/Adrenokrome/int-syst/uploads/197fac76496df2f76b147254886c9a20/network_Nat.PNG "Network")

-------------------------------------------------------------

### Installation Httpd et configuration  

- Activation du httpd

    systemctl start httpd  
    systemctl is-enabled httpd
  
- Parametrage du firewall pour permettre l'accès au http   

    firewall-cmd --zone=public --add-port=http/tcp --permanent
    firewall-cmd --zone=public --add-port=https/tcp --permanent

----------------------------------------------------------------

### Installation de Dokuwiki dans /var/www/html


    cd /var/www/html
    
- On télécharge la dernière version de DokuWiki: http://download.dokuwiki.org/


    wget http://download.dokuwiki.org/out/dokuwiki-63487079d8919ad20087d39beea025a9.tgz
    
    
- On décompresse l'archive et on la supprime une fois terminé.


    tar -zxf dokuwiki-63487079d8919ad20087d39beea025a9.tgz 
    rm -rf dokuwiki-63487079d8919ad20087d39beea025a9.tgz
   
- On modifie les permissions.


    chown -R apache:root /var/www/html/dokuwiki     
    chmod -R 664 /var/www/html/dokuwiki/    
    find /var/www/html/dokuwiki/ -type d -exec chmod 775 {} \;    
    
    
- Nous allons ensuite créer un virtualhost pour un accéder à DokuWiki.


    vi /etc/httpd/conf.d/dokuwiki.conf     
       
    
![dokuwi.conf](/uploads/a29db27926459602f62366321cecd37f/dokuwi.conf.PNG "dokuwi.conf")

- On verifie que la commande c'est bien executé


    apachectl configtest  
    
- On réinitialise le service httpd


    systemctl restart httpd   


- On desactive les droits selinux afin de pouvoir avoir l'accès au fichier du Dokuwiki


    setenforce 0
        
- Et on modifie le fichier /etc/selinux/config.


    nano /etc/selinux/config
    SELINUX=enforcing -> SELINUX=disabled
    

- Depuis votre navigateur,vous pouvez maintenant accéder à l'installe du DokuWiki.


    http:// 10.0.2.15/install.php  
    
    
L'adresse IP est celle de ma VM.

![Doku_Installer](/uploads/a8e91808f1c860c3b59c01c4e2fe0471/Doku_Installer.png "Doku_Installer")

-----------------------------------------------------------------

### Partionnement d'une partition de secours       

Mise en place d'une partion dédier au donnée à sauvegarder du serveur web 

Rajouté un disque dur de 10Go nommé Backup_Data_Serv  
Formaté en EXT4 et monté  
J'ai decidé de ne pas partager le même LVM en cas de problème d'un disque dur  

![Stockage](/uploads/df67760297a2a80b2aa30a453692ab41/Stockage.PNG "Stockage")

Liste des disques durs sur la VM :

    lsblk
    
Partionnement du disque dur :

    fdisk /dev/sdb
 
Formatage en EXT4 : 

    mkfs.ext4 /dev/sdb1

Création du dossier pour le montage du disque dur :

    mkdir /sauvegarde
    
Puis j'ajoute cette ligne dans /etc/fstab :

    /dev/sdb1 /sauvegarde ext4 defaults, 0        0
    
![fstab](/uploads/ba99510a3c2dbca6b27b93ef289de7a8/fstab.PNG "fstab")

Pour prendre en compte les modifications du fstab

    mount -a
    
------------------------------------------------------------------

### Mise en place de la sauvegarde (Wikibackup.sh)

J'ai écrit un script afin de réaliser ma sauvegarde

    #!/bin/bash  
        rsync -av /etc/httpd/conf.d/dokuwiki.conf /sauvegarde/save.conf  
        rsync -av /var/www/html/dokuwiki /sauvegarde/save.dokuwiki  
    if [ "$?" = "0" ]  
    then  
        echo "Save OK" | tee -a /root/log_srv/savesrv.log  
    else  
        echo "Save Erreur" | tee -a /root/log_srv/savesrv.log  
    fi  

J'ai ensuite implanté mon script dans crontab    

    crontab -e   
    0 20 */2 * * ./Wikibackup.sh >> /root/log_srv/cron.log 2>&1
    
Mon crontab permet d'effectuer ma sauvegarde tout les 2 jours à 20h00.
    
J'ai ensuite vérifié dans mes fichiers log savesrv.log et cron.log si mon programme s'exécuté bien. 

---------------------------------------------------------------------------

### Mettre en place la connexion à distance (Forward de port sur la livebox)  

J'ai configuré ma Freebox afin de rediriger toutes les connexions entrantes de ma freebox vers le PC qui heberge mon serveur.
Toutes les adresses IP qui vont sur l'adresse :

    91.164.232.75:35000
    
Sont rédiriger vers le PC :

    192.168.0.25 sur le port 800
    
Afin de garantir la sécurité de mon réseaux local, j'ai modifier l'IP source en lui mettant l'adresse IP de mon smartphone.

![Config_FreeBox](/uploads/61bfdb2cd5e9a51c36a21463fb379c0d/Config_FreeBox.PNG "Config_FreeBox")

--------------------------------------------------------------------------------

### Forward de port sur Virtual Box pour que le PC hôte accède à la VM

Pour que mon Pc Hôte redirige les connexions entrantes de ma Freebox vers ma VM, j'ai effectuer la configuration suivante:

![forward_NAT](/uploads/cd221769813e4e0ebfeedd0eb85fb32f/forward_NAT.PNG "forward_NAT")


--------------------------------------------------------------------------

### Mise en place de la page Wiki            

Nous pouvons maintenant éditer et configurer notre page wiki comme bon nous semble.

![dokuwiki_edit](/uploads/4f50bdc1028608e3e6234fddb940087e/dokuwiki_edit.PNG "dokuwiki_edit") 
------------------------------------------------------------------------

### Amelioration de mon Serveur en terme de sécurité (option)

- Sécuriser mon serveur Apache avec Fail2Ban             

J'ai copier le fichier de configuration afin de ne pas écraser l'ancien sous fausse manipulation           

    cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local    
    
Impossible de le mettre en place avec ma configuration :               
Mise en place d'un brigde sur Virtual Box afin de supprimer une couche pour voir les IP des connexions entrantes.                    
Seul l'ip de mon PC Windows est vu par ma VM.                   
Voir fichier log http access pour voir les IP à autoriser pour continuer ma configuration.                  
    
- Mise en place de Guacamole

Problème de certificat SSL sur le fichier ssl.conf

- Mise en place d'un DNS

Fichier Host impossible car adresse ip avec possédant un port              
Dyndns payant             

### Conclusion

J'ai developpé mes compétences en réseau à distance et pris le goût d'apprendre l'HTML afin de créer mes propres sites personnel.                 
J'aurai l'utilité de me servire de mon Dokuwiki dans le milieu professionel.                  





