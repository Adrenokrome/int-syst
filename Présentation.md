# Présentation de mon materiel et ma configuration 


## PC OMEN by HP Laptop 15-dc0010nf  

Marque d'origine : HP  
Référence : 15-DC 0010 OMEN  
Système d'exploitation : Windows 10 Entreprise LTSC  
Modèle de processeur : Intel Core i7  
Numéro de Processeur : i7-8750H  
Capacité du disque dur : 1 To  
Stockage hybride : Oui (1 To + 128 Go SSD)  
Mémoire vive installée (Go) : 8  
Carte graphique : Nvidia GeForce GTX1050  
Mémoire Vidéo en Mb : 4 Go  
Marque du Chipset : Intel  
Type de Chipset : Intel HM370  
Fréquence du processeur (GHz) : 2.2-4.1  








