# [Présentation](./Présentation.md)

Présentation de mon materiel et ma configuration

# [Sécuriser et fiabiliter de mon laptop](./Sécuriser-fiabilité-laptop.md)

Mise en place de sauvegarde afin de garantir la fiabilité

# [Projet Server WEB](./Projet-Server-WEB.md)

Serveur WEB avec Dokuwiki à distance

### Installation de la machine CentOS
### Configuration de CentOS et de ses cartes réseaux
### Installation Httpd et configuration
### Installation de Dokuwiki dans /var/www/html
### Partionnement d'une partition de secours
### Mise en place de la sauvegarde (Wikibackup.sh)
### Mettre en place la connexion à distance (Forward de port sur la livebox)
### Forward de port sur Virtual Box pour que le PC hôte accède à la VM
### Mise en place de la page Wiki
### Amelioration de mon Serveur en terme de sécurité (option)
### Conclusion

# [Document d'exploitation utilisation système](./Document-d'exploitation-utilisation-système.md)

Prise en main et utilisation du système Server Web