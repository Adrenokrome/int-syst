# Mise en place de sauvegarde afin de garantir la fiabilité :

- Mise en place de deux disques durs séparés DISQUE LOCAL et DATA.

- Exportation d'un fichier image de ma configuration PC manuellement via le l'utilitaire de récupération Windows. 

- Sauvegarde du dossier "Documents" via le logiciel SyncTrayzor vers un Pc serveur.
 
- Sauvegarde du dossier "Documents" via le logiciel One Drive vers un cloud.

- Cryptage de donnée afin de garantir la sécurité de mes données en projet.

